/**
 * Copyright (c) 2021 Christian Ege.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

// System includes
#include <stdio.h>

#include <string>
#include <vector>

// SDK includes
#include "pico/stdlib.h"
#include "pico/unique_id.h"

// third party lib includes
#include <fmt/core.h>
#include <fmt/format.h>

// local includes
#include "version_info.h"

static std::string getBoardID() {
  pico_unique_board_id_t board_id;
  pico_get_unique_board_id(&board_id);
  const std::vector<uint8_t> unique_id(
      board_id.id, board_id.id + PICO_UNIQUE_BOARD_ID_SIZE_BYTES);
  return fmt::format("{:02x}", fmt::join(unique_id, ""));
}

int main() {
  stdio_init_all();
  const auto BOARD_ID{getBoardID()};
  while (true) {
    fmt::print("Welcome to: {} version: {}\n", PROJECT_NAME, PROJECT_VER);
    fmt::print("Our unique board ID is: {}\n", BOARD_ID);
    sleep_ms(1000);
  }
  return 0;
}
