#!/bin/bash

sudo apt-get update
sudo apt-get install --no-install-recommends --yes \
        cmake \
        clang-format \
        ninja-build \
        bash \
        gcc-arm-none-eabi \
        libnewlib-arm-none-eabi \
        libstdc++-arm-none-eabi-newlib

# Get the pico-sdk
git clone -b ${PICO_SDK_VERSION} https://github.com/raspberrypi/pico-sdk.git "/opt/sdk/pico-sdk_${PICO_SDK_VERSION}"
cd ${PICO_SDK_PATH} && git submodule init && git submodule update
