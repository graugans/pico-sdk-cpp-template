# The Raspberry Pi Pico C++ template for Visual Studio Code development

This a template project for developing application which run on the Raspberry Pi Pico Microcontroller board. By default it is using the C++ Language for development. This may not produce the smallest possible executables. But it is the language of my choice when it comes to programming.

# License and Copyright

This template, is Copyright © 2021 Christian Ege. and licensed under the 3-Clause BSD license.
Please also check the licenses of the third party libs under the lib folder and the CMake files from the pico-sdk for their license.